
#!/bin/sh

LOG_FILE="csf-check.txt"
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="N/A"

/bin/echo "=============================================================="
/bin/echo "[csf] Config Server Firewall is checking for latest update ..."

/usr/sbin/csf -c > /tmp/$LOG_FILE 2>&1

check_for_update() {

        if grep -q newer "/tmp/$LOG_FILE"

        then
                WARNING_STATUS="WARNING"
                /bin/echo "[csf] New version available."

                /bin/mail -s "[csf][WARNING|N] Config File Server Update Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$LOG_FILE
        else
                WARNING_STATUS="OK"
                echo "[csf] Already have the latest version."
                /bin/mail -s "[csf][OK|N] Config File Server Update Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$LOG_FILE
        fi
}

check_for_update

/bin/rm /tmp/$LOG_FILE
/bin/echo "[csf] Check status: $WARNING_STATUS"
/bin/echo "[csf] Done checking. Email is set to be sent to $MYEMAIL"
/bin/echo "=============================================================="
